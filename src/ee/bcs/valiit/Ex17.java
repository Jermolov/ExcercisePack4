package ee.bcs.valiit;

public class Ex17 {
    public static void main(String[] args) {
        StringBuilder resultat=new StringBuilder();
        for (int i=0;i<args.length;i++) {
            switch (args[i]) {
                case "0":
                    resultat.append("null");
                    break;
                case "1":
                    resultat.append("one");
                    break;
                case "2":
                    resultat.append("two");
                    break;
                default:
                    break;
                }
                if(args.length>i+1){
                    resultat.append(", ");
                }
        }System.out.println(resultat);
    }
}
