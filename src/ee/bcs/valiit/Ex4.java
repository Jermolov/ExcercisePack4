package ee.bcs.valiit;

public class Ex4 {
    public static void main(String[] args) {
        int answer=3;
        if(answer>5 || answer<1){
            System.out.println("Number should be from 1 to 5");
        }

        switch (answer){
            case 1: System.out.println("Bad");
                break;

            case 2: System.out.println("Better");
                break;

            case 3: System.out.println("Even better");
                break;

            case 4: System.out.println("Almost");
                break;

            case 5: System.out.println("The best connection");
                break;

            default: System.out.println("This Switch checks connection");
                break;
        }
    }
}
