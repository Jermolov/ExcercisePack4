package ee.bcs.valiit;

public class Ex3 {
    public static void main(String[] args) {
        int number=3;
        if(number>5 || number<1){
            System.out.println("Number should be from 1 to 5");
        }

        if(number==1){
            System.out.println("Wikness");
        }else if(number==2){
            System.out.println("not good");
        }else if(number==3){
            System.out.println("ok");
        }else if(number==4){
            System.out.println("good");
        }else if(number==5){
            System.out.println("perfect");
        }else{
            System.out.println("Nothing");
        }
    }
}
