package ee.bcs.valiit;

import java.util.Iterator;
import java.util.TreeSet;

public class Ex21 {
    public static void main(String[] args) {
        TreeSet<String>names=new TreeSet<>();
        names.add("First");
        names.add("Second");
        names.add("Third");

        Iterator<String>it=names.iterator();
        for (String i:names) {
            System.out.println(it.next());
        }
    }
}
