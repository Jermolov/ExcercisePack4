package ee.bcs.valiit;

public class Ex16 {
    public static void main(String[] args){
        String[]bands=new String[]{"Sun","Metsatoll","Queen","Metallica"};
        for(int i=0;i<bands.length/2;i++){
            String temp=bands[i];
            bands[i]=bands[bands.length-i-1];
            bands[bands.length-i-1]=temp;
        }
        for (int j=0;j<bands.length;j++){
            System.out.println(bands[j]);
        }
    }
}
